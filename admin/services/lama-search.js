export function LamaSearch(obj) {
  let callBack = obj.callBack == undefined ? () => {} : obj.callBack;

  const icon = document.querySelector(".lama-icon");
  const search = document.querySelector(".lama-search");
  const mySearch = document.querySelector("#lama-my-search");
  const clear = document.querySelector(".lama-clear");

  mySearch.onkeyup = (e) => {
    e.key == "Enter" && callBack(mySearch.value);
  };
  icon.onclick = () => {
    search.classList.toggle("active");
  };
  clear.onclick = () => {
    mySearch.value = "";
  };
}
