import {
  hideLoading,
  showLoading,
} from "../../customer/controller/controller.js";
import { LamaSearch } from "./lama-search.js";
import { newProduct, productTypes } from "../../customer/model/products.js";
import {
  getFormData,
  renderTable,
  setFormData,
} from "../controller/controller.js";
import {
  isValidBackCamera,
  isValidDescription,
  isValidFrontCamera,
  isValidImageURL,
  isValidName,
  isValidPrice,
  isValidScreen,
  isValidType,
} from "./validate.js";

const BASE_URL = "https://63ff4480571200b7b7d9c6f1.mockapi.io";
const myModal = new bootstrap.Modal("#productDetail", { keyboard: true });
let productList = [];
let isAscending = true;

const fetchData = (key) => {
  showLoading();
  axios({
    url: `${BASE_URL}/products`,
    method: "GET",
  })
    .then((res) => {
      productList = [];
      res.data.map((product) => {
        if (product.name.toLowerCase().indexOf(key.toLowerCase()) != -1)
          productList.push(newProduct(product));
      });
      document.getElementById("sort-order").innerHTML = "";
      renderTable(productList);
      hideLoading();
    })
    .catch((err) => {
      console.log(err);
      hideLoading();
    });
};
fetchData("");

document.getElementById("product-type").innerHTML += productTypes
  .map((type) => `<option value="${type}">${type}</option>`)
  .join("");

LamaSearch({
  callBack: (value) => {
    fetchData(value);
  },
});

const sortPrice = () => {
  productList.sort((a, b) => {
    return isAscending ? a.price - b.price : b.price - a.price;
  });
  document.getElementById(
    "sort-order"
  ).innerHTML = ` <i class="fa fa-sort-numeric-${
    isAscending ? "up" : "down"
  }"></i>`;
  isAscending = !isAscending;
  renderTable(productList);
};

document.getElementById("sort-price").onclick = () => {
  sortPrice();
};

window.resetForm = () => {
  document.getElementById("form").reset();
};

const setupForm = (isAddProduct = true) => {
  resetForm();
  Array.from(document.getElementsByClassName("warning")).forEach((element) =>
    element.removeAttribute("style")
  );
  document.getElementById("add-product").style.display = isAddProduct
    ? "block"
    : "none";
  document.getElementById("update-product").style.display = isAddProduct
    ? "none"
    : "block";
};

document.getElementById("show-add-product").onclick = () => {
  setupForm();
};

const isValidate = (data) => {
  let isValid = isValidName(data.name, "sp-product-name");
  isValid = isValid & isValidPrice(data.price, "sp-product-price");
  isValid = isValid & isValidScreen(data.screen, "sp-product-screen");
  isValid =
    isValid & isValidBackCamera(data.backCamera, "sp-product-back-camera");
  isValid =
    isValid & isValidFrontCamera(data.frontCamera, "sp-product-front-camera");
  isValid = isValid & isValidImageURL(data.img, "sp-product-img");
  isValid = isValid & isValidDescription(data.desc, "sp-product-desc");
  isValid = isValid & isValidType(data.type, "sp-product-type");
  return isValid;
};

window.addProduct = () => {
  let data = getFormData();
  data.id = null;
  if (!isValidate(data)) return;
  myModal.hide();
  showLoading();
  axios({
    url: `${BASE_URL}/products`,
    method: "POST",
    data: data,
  })
    .then((res) => {
      console.log(res.data);
      fetchData("");
    })
    .catch((err) => {
      console.log(err);
      hideLoading();
    });
};

window.editProduct = (id) => {
  showLoading();
  axios({
    url: `${BASE_URL}/products/${id}`,
    method: "GET",
  })
    .then((res) => {
      console.log(res.data);
      hideLoading();
      setupForm(false);
      setFormData(res.data);
      myModal.show();
    })
    .catch((err) => {
      console.log(err);
      hideLoading();
    });
};

window.updateProduct = () => {
  let data = getFormData();
  if (!isValidate(data)) return;
  myModal.hide();
  showLoading();
  axios({
    url: `${BASE_URL}/products/${data.id}`,
    method: "PUT",
    data: data,
  })
    .then((res) => {
      console.log(res.data);
      fetchData("");
    })
    .catch((err) => {
      console.log(err);
      hideLoading();
    });
};

window.deleteProduct = (id) => {
  showLoading();
  axios({
    url: `${BASE_URL}/products/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      console.log(res.data);
      fetchData("");
    })
    .catch((err) => {
      console.log(err);
      hideLoading();
    });
};
