import { productTypes } from "../../customer/model/products.js";

const showMessage = (id, message) => {
  document.getElementById(id).style.display = "block";
  document.getElementById(id).innerText = message;
};

export const isValidName = (name, id) => {
  let regex = /\w+/g;
  if (regex.test(name)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Tên sản phẩm không được để trống");
    return false;
  }
};

export const isValidPrice = (price, id) => {
  let regex = /^[1-9]\d{2,}$/g;
  if (regex.test(price)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Giá sản phẩm phải là số");
    return false;
  }
};

export const isValidScreen = (screen, id) => {
  let regex = /.+/g;
  if (regex.test(screen)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Màn hình không được để trống");
    return false;
  }
};

export const isValidBackCamera = (backCamera, id) => {
  let regex = /.+/g;
  if (regex.test(backCamera)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Camera sau không được để trống");
    return false;
  }
};

export const isValidFrontCamera = (frontCamera, id) => {
  let regex = /.+/g;
  if (regex.test(frontCamera)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Camera trước không được để trống");
    return false;
  }
};

export const isValidImageURL = (img, id) => {
  let regex = /.+/g;
  if (regex.test(img)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "URL hình ảnh không đúng định dạng");
    return false;
  }
};

export const isValidDescription = (desc, id) => {
  let regex = /.+/g;
  if (regex.test(desc)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Mô tả không được để trống");
    return false;
  }
};

export const isValidType = (type, id) => {
  let types = productTypes.map((type) => type).join("|");
  let regex = new RegExp(`${types}`, "g");
  if (regex.test(type)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Chưa chọn loại sản phẩm");
    return false;
  }
};
