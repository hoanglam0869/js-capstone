import {
  formatCurrency,
  getRandomNumber,
} from "../../customer/controller/controller.js";
import { Products } from "../../customer/model/products.js";

export const renderTable = (productList) => {
  document.getElementById("product-management").innerHTML = productList
    .map(
      (product) => `
  <tr>
    <th scope="row">${product.id}</th>
    <td>${product.name}</td>
    <td><strong class="text-danger">${formatCurrency(
      product.price
    )}</strong></td>
    <td><img class="w-100" style="max-width: 200px" src="${product.img}" alt="${
        product.name
      }"/></td>
    <td>${
      product.desc.length > 50
        ? product.desc.slice(0, 50) + "..."
        : product.desc
    }</td>
    <td>
      <button class="btn btn-warning text-white d-flex align-items-center" onclick="editProduct(${
        product.id
      })">Sửa<i class="fa fa-pen-square ms-2"></i></button>
      <button class="btn btn-danger text-white d-flex align-items-center" onclick="deleteProduct(${
        product.id
      })">Xóa<i class="fa fa-trash ms-2"></i></button>
    </td>
  </tr>`
    )
    .join("");
};

export const getFormData = () => {
  let id = document.getElementById("product-id").value;
  let name = document.getElementById("product-name").value;
  let price = document.getElementById("product-price").value * 1;
  let screen = document.getElementById("product-screen").value;
  let backCamera = document.getElementById("product-back-camera").value;
  let frontCamera = document.getElementById("product-front-camera").value;
  let img = document.getElementById("product-img").value;
  let desc = document.getElementById("product-desc").value;
  let type = document.getElementById("product-type").value;
  let rating = getRandomNumber(1, 10) / 2;

  return new Products(
    id,
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type,
    rating
  );
};

export const setFormData = (product) => {
  document.getElementById("product-id").value = product.id;
  document.getElementById("product-name").value = product.name;
  document.getElementById("product-price").value = product.price;
  document.getElementById("product-screen").value = product.screen;
  document.getElementById("product-back-camera").value = product.backCamera;
  document.getElementById("product-front-camera").value = product.frontCamera;
  document.getElementById("product-img").value = product.img;
  document.getElementById("product-desc").value = product.desc;
  document.getElementById("product-type").value = product.type;
};
