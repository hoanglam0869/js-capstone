const renderRating = (rating) => {
  let contentHTML = "";
  const stars = Math.floor(rating);
  for (let i = 0; i < 5; i++) {
    if (i < stars) contentHTML += '<i class="fa fa-star text-warning"></i>';
    else if (rating - i == 0.5)
      contentHTML += '<i class="fa fa-star-half-alt text-warning"></i>';
    else contentHTML += '<i class="fa fa-star text-dark"></i>';
  }
  return contentHTML;
};

export const getRandomNumber = (min, max) =>
  Math.floor(Math.random() * (max - min + 1)) + min;

export const formatCurrency = (price) => {
  return new Intl.NumberFormat("vi-VI", {
    style: "currency",
    currency: "VND",
  }).format(price);
};

export const renderProducts = (productList, currentPage, limitPerPage) => {
  const start = (currentPage - 1) * limitPerPage;
  const end =
    start + limitPerPage > productList.length
      ? productList.length
      : start + limitPerPage;
  let contentHTML = "";
  for (let i = start; i < end; i++) {
    const product = productList[i];
    contentHTML += `<div class="wrapper position-relative p-3 col-xl-3 col-lg-4 col-md-6 col-sm-12">
    <div class="card p-2 h-100 text-center">
      <img class="card-img-top w-100" src="${product.img}" alt="${
      product.name
    }">
      <div class="card-body">
        <h5 class="card-title">${product.name}</h5>
        <strong class="card-title text-danger">${formatCurrency(
          product.price
        )}</strong><br>
        <p class="bg-secondary rounded-pill text-white d-inline-block px-2 my-1">${
          product.type
        }</p>
        <p class="card-text">${
          product.desc.length > 50
            ? product.desc.slice(0, 50) + "..."
            : product.desc
        }</p>
        ${
          product.rating == 0
            ? ""
            : `<div>${renderRating(product.rating)} <span>${getRandomNumber(
                0,
                500
              )}</span></div>`
        }
      </div>
    </div>
    <div class="overlay position-absolute top-0 start-0 end-0 bottom-0 p-2 m-3 rounded text-white text-center">
      <h5>Thông số kỹ thuật</h5>
      <div class="specifications text-start h-75">
        <p class="screen"><strong>Screen:</strong> ${product.screen}</p>   
        <p class="back-camera"><strong>Back Camera:</strong> ${
          product.backCamera
        }</p>   
        <p class="front-camera"><strong>Front Camera:</strong> ${
          product.frontCamera
        }</p>
        <p class="desc"><strong>Description:</strong> ${product.desc}</p>
      </div>
      <div class="h-25">
        <button class="add-to-cart btn btn-warning mt-4" onclick="addToCart(${
          product.id
        })">Thêm giỏ hàng</button>
      </div>
    </div>
</div>
`;
  }
  document.getElementById("product-list").innerHTML = contentHTML;
};

export const renderCart = (cartArray) => {
  let totalQuantities = 0;
  let totalPay = 0;
  document.getElementById("cart-list").innerHTML =
    cartArray.length == 0
      ? `<tr class="text-center border border-white">
      <td>
        <p>Giỏ hàng trống</p>
        <button class="btn btn-success" data-bs-dismiss="modal">
          Mua ngay
        </button>
      </td>
    </tr>`
      : cartArray
          .map((item) => {
            totalQuantities += item.quantity;
            let price = item.quantity * item.product.price;
            totalPay += price;
            return `<tr><td>
        <div class="row">
          <div class="col-4">
            <img class="w-100" src="${item.product.img}" alt="${
              item.product.name
            }"/>
          </div>
          <div class="col-6">
            <h5 class="name">${item.product.name}</h5>
            <div class="screen"><strong>Screen:</strong> ${
              item.product.screen
            }</div>   
            <div class="back-camera"><strong>Back Camera:</strong> ${
              item.product.backCamera
            }</div>   
            <div class="front-camera"><strong>Front Camera:</strong> ${
              item.product.frontCamera
            }</div>
          </div>
          <div class="col-2 d-flex justify-content-center align-items-center">
            <button class="btn btn-danger" onclick=deleteCartItem(${
              item.product.id
            })><i class="fa fa-trash fs-4"></i></button>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-4 fs-4">
            <a class="text-primary" onclick="changeQuantity(${
              item.product.id
            }, -1)"><i class="fa fa-minus-circle"></i></a>
            <strong>${item.quantity}</strong>
            <a class="text-primary" onclick="changeQuantity(${
              item.product.id
            }, 1)"><i class="fa fa-plus-circle"></i></a>
          </div>
          <div class="col-8 d-flex justify-content-end align-items-center">
            <strong class="card-title text-danger">${formatCurrency(
              price
            )}</strong>
          </div>
        </div>
      </td></tr>`;
          })
          .join("");
  document.getElementById("quantity").innerText = totalQuantities;
  document.getElementById("total-pay").innerText = formatCurrency(totalPay);
};

export const showLoading = () => {
  document.getElementById("loading").style.display = "flex";
};

export const hideLoading = () => {
  document.getElementById("loading").style.display = "none";
};

export const showToast = (message, isErr = false) => {
  Toastify({
    text: message,
    duration: 3000,
    destination: "https://github.com/apvarun/toastify-js",
    newWindow: true,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "center", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: isErr
        ? "linear-gradient(25deg,#d64c7f,#ee4758 50%)"
        : "linear-gradient(to right, #00b09b, #96c93d)",
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};
