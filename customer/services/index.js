import {
  hideLoading,
  renderCart,
  renderProducts,
  showLoading,
  showToast,
} from "../controller/controller.js";
import { Cart, CartItem } from "../model/cart.js";
import { newProduct, productTypes } from "../model/products.js";
import { LamaPagination } from "./lama-pagination.min.js";

const BASE_URL = "https://63ff4480571200b7b7d9c6f1.mockapi.io";
const CART_ARRAY_KEY = "CART_ARRAY_KEY";

let productList = [];
let cart = new Cart();

document.getElementById("product-type").innerHTML += productTypes
  .map((type) => `<option value="${type}">${type}</option>`)
  .join("");

let jsonData = localStorage.getItem(CART_ARRAY_KEY);
if (jsonData != null) {
  cart.cartArray = JSON.parse(jsonData).map(
    (item) => new CartItem(newProduct(item.product), item.quantity)
  );
  renderCart(cart.cartArray);
}

// PAGINATION
const pagination = () => {
  LamaPagination({
    paginationSize: 7,
    limitPerPage: 12,
    itemCount: productList.length,
    prev: "Trước",
    next: "Sau",
    callBack(currentPage, limitPerPage) {
      renderProducts(productList, currentPage, limitPerPage);
    },
  }).setupPagination(1);
};

const fetchData = (key) => {
  showLoading();
  axios({
    url: `${BASE_URL}/products`,
    method: "GET",
  })
    .then((res) => {
      productList = [];
      res.data.forEach((product) => {
        if (product.type.toLowerCase().indexOf(key.toLowerCase()) != -1)
          productList.push(newProduct(product));
      });
      pagination();
      hideLoading();
    })
    .catch((err) => {
      console.log(err);
      hideLoading();
    });
};

fetchData("");

document.getElementById("product-type").onchange = () => {
  const value = document.getElementById("product-type").value;
  fetchData(value);
};

const saveCart = () => {
  let dataJSON = JSON.stringify(cart.cartArray);
  localStorage.setItem(CART_ARRAY_KEY, dataJSON);
};

window.addToCart = (id) => {
  let index = productList.findIndex((product) => product.id == id);
  let cartItem = new CartItem(productList[index], 1);
  cart.addToCart(cartItem);
  renderCart(cart.cartArray);
  saveCart();
  showToast("Thêm giỏ hàng thành công");
};

window.deleteCartItem = (id) => {
  cart.deleteCartItem(id);
  renderCart(cart.cartArray);
  saveCart();
  showToast("Xóa sản phẩm thành công");
};

window.changeQuantity = (id, quantity) => {
  cart.changeQuantity(id, quantity);
  renderCart(cart.cartArray);
  saveCart();
};

window.pay = () => {
  if (cart.cartArray.length == 0) {
    showToast("Mua hàng để thanh toán", true);
    return;
  }
  cart.cartArray = [];
  renderCart(cart.cartArray);
  saveCart();
  showToast("Thanh toán thành công");
};

window.deleteAll = () => {
  cart.cartArray = [];
  renderCart(cart.cartArray);
  saveCart();
  showToast("Làm trống giỏ hàng");
};
