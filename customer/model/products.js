export class Products {
  constructor(
    id,
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type,
    rating
  ) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.screen = screen;
    this.backCamera = backCamera;
    this.frontCamera = frontCamera;
    this.img = img;
    this.desc = desc;
    this.type = type;
    this.rating = rating;
  }
}

export const newProduct = (product) =>
  new Products(
    product.id,
    product.name,
    product.price,
    product.screen,
    product.backCamera,
    product.frontCamera,
    product.img,
    product.desc,
    product.type,
    product.rating
  );

export const productTypes = [
  "Samsung",
  "OPPO",
  "iPhone",
  "Xiaomi",
  "Vivo",
  "Realme",
  "POCO",
  "TCL",
  "Nokia",
];
