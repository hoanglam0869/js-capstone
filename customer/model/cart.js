export class Cart {
  constructor() {
    this.cartArray = [];
  }
  addToCart(cartItem) {
    let index = this.findIndex(cartItem.product.id);
    if (index == -1) {
      this.cartArray.push(cartItem);
    } else {
      this.cartArray[index].quantity++;
    }
  }
  findIndex(id) {
    return this.cartArray.findIndex((item) => item.product.id == id);
  }
  deleteCartItem(id) {
    let index = this.findIndex(id);
    this.cartArray.splice(index, 1);
  }
  changeQuantity(id, quantity) {
    let index = this.findIndex(id);
    this.cartArray[index].quantity += quantity;
    this.cartArray[index].quantity == 0 && this.cartArray.splice(index, 1);
  }
}

export class CartItem {
  constructor(product, quantity) {
    this.product = product;
    this.quantity = quantity;
  }
}
