// Product list
const li = document.getElementsByClassName(" item ajaxed __cate_42");

for (let i = 0; i < li.length; i++) {
  const imgTag = li[i].getElementsByClassName("thumb");
  const img = imgTag[0].getAttribute("src");
  const h3Tag = li[i].getElementsByTagName("h3");
  const name = h3Tag[0].innerText;
  const strongTag = li[i].getElementsByClassName("price");
  const price = strongTag[0].innerText.replace(/[\.₫]/g, ""); //21.990.000₫
  console.log(`${name}\t${price}\t${img}`);
}
